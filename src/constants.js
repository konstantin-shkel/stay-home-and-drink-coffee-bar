export const rooms = [
  { id: 'room–1', name: 'Speak’n joy club', description: 'The room for those who wanna speak English. Don’t be shy – we’re all here to help each other to improve our skills.', joinLink: 'https://ecsc00a07a4f.epam.com/SpeaknJoyClub'},  
  { id: 'room–2', name: 'Water cooler talks', description: "Do you remember those moments and conversations next to water coolers and coffee machines? – So this is pretty the same. But online 😊", joinLink: 'https://ecsc00a07a4f.epam.com/WaterCoolerTalks'},
  { id: 'room–3', name: 'Movie trailer', description: "Ideal place for movie lovers, serials admires to discuss the latest news in the world of cinema!", joinLink: 'https://ecsc00a07a4f.epam.com/MovieTrailer'},
  { id: 'room–4', name: 'Lounge zone', description: 'Grab your cup of hot beverage, relax, and feel free to raise any topic you want.', joinLink: ''},
  { id: 'room–5', name: 'Fun Time', description: 'Have some hobby you wanna share with colleagues? Wanna teach/learn smth or find like-minded people? – Here you go.', joinLink: ''}
];

export const advices = [
  { title: 'Starting Conversation', text: "Try to find a pause in the dialog before saying 'Hi' and don’t forget to add something to support the topic (of course, if you already waited long enough to catch it) as it will keep conversation going." },
  { title: 'Newbie is joining', text: "If you’ve noticed newcomers joining the chat – make a pause to greet them, introduce the topic and ask a question – it will help you to involve everyone into the discussion and make lots of new connections." },
  { title: 'Stay positive', text: "Remember to stay positive, as your emotions might be really infective. Let's make this place be a good mood charger 😊" },
  { title: 'Leaving conversation', text: "Excuse before leaving and thank everyone for making this conversation really interesting and useful (add your own evaluation here :)" },
  { title: 'Leaving conversation (part 2)', text: "If somebody left silently — it's not about you, it's about meetings or connection troubles. In other cases the colleague would have said good-bye and thanked for a great conversation, haven’t they? 😊" },
  { title: 'Stay open', text: "Every person is interesting by default as well as there’re no uninteresting topics or points of view, so don’t be shy, share your story, experience, opinion – this is an open conversation." },
  { title: 'Camera is on', text: "When chatting it’s really essential (especially nowadays) to see other people nodding/smiling/showing their intention to tell smth, so please switch your camera on show your pet/favorite cup/plant, etc. put on a mask – it will help to make your conversation informal, friendly and lamp." }
];