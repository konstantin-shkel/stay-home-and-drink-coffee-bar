import styled from 'styled-components' 

import { BaseTextColor, BaseTextFamily } from './constants';

export const BaseTextSection = styled.section`
  color: ${BaseTextColor};
  font-family: ${BaseTextFamily};
  font-size: 16px;
  line-height: 24px;
`;