import styled from 'styled-components';

export const PageContainer = styled.div`
  width: 100%;
  height: auto;
  padding-top: 40px;
  padding-bottom: 40px;
`;