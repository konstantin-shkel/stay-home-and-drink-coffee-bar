import styled from 'styled-components';

import { Link } from "react-router-dom";

import { BaseTextFamily } from './constants';

export const NavigationLink = styled(Link)`
  padding-top: 30px;
  color: #008ACE;
  font-size: 16px;
  font-family: ${BaseTextFamily};
  text-decoration: none;
  display: block;
`;
