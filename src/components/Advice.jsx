import React from 'react'
import styled from 'styled-components' 

import { BaseTextSection } from './BaseTextSection';
import { BaseBorderColor } from './constants';

const Number = styled.div`
  border-bottom: 1px solid ${BaseBorderColor};
  padding-bottom: 10px;
`;

const Title = styled.div`
  font-size: 22px;
  line-height: 26px;
  font-weight: bold;
  padding-top: 10px;
  padding-bottom: 7px;
`;

const Text = styled.div`
  padding-right: 15px;
  word-wrap: break-word;
`;

export function Advice({ number, title, text }) {  
  return (
    <BaseTextSection>
      <Number>Advice #{number}.</Number>
      <Title>{title}</Title>
      <Text>{text}</Text>
    </BaseTextSection>
  );
}
