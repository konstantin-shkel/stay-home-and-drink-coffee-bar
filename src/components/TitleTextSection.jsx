import styled from 'styled-components';

import { BaseTextSection } from './BaseTextSection';

export const TitleTextSection = styled(BaseTextSection)`
  font-size: 90px;
  line-height: 108px;
  font-weight: bold;
`;