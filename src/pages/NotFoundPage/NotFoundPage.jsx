import React from 'react';
import styled from 'styled-components';

import { NavigationLink } from '../../components/NavigationLink';
import { TitleTextSection } from '../../components/TitleTextSection';
import { Link } from 'react-router-dom';

const PageWrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-content: center;
  justify-content: center;
  height: 100%;
`;

const LineBreak = styled.div`
  width: 100%;
  margin-top: 8px;
  margin-bottom: 8px;
`;

export function NotFoundPage({ number, title, text }) {  
  return (
    <PageWrapper>
      <TitleTextSection>Page Not Found</TitleTextSection>
      <LineBreak />
      <NavigationLink to="/">← BACK TO HOME</NavigationLink>
    </PageWrapper>
  );
}
