import React from 'react';
import styled from 'styled-components';

import { NavigationLink } from '../../components/NavigationLink';
import { TitleTextSection } from '../../components/TitleTextSection';
import { Advice } from '../../components/Advice';

import { advices } from '../../constants';

const HeaderWrapper = styled.div`
  padding-left: 15%;
`;

const TitleText = styled(TitleTextSection)`
  padding-top: 20px;
`;

const Body = styled.div`
  display: flex;
  padding-right: 10%;
  padding-left: 30%;
  flex-wrap: wrap;
  margin-top: 50px;
`;

const AdviceColumn = styled.div`
  flex: 0 0 46%;
  padding-right: 4%;
  padding-bottom: 40px;
`;

export function AdvicesPage({ number, title, text }) {  
  return (
    <>
      <HeaderWrapper>
        <NavigationLink to="/">← BACK TO HOME</NavigationLink>
        <TitleText>All Advices</TitleText>
      </HeaderWrapper>
      <Body>
        {
          advices.map(({ title, text }, index) => (
            <AdviceColumn>
              <Advice key={index} number={index + 1} title={title} text={text} />
            </AdviceColumn>
          ))
        }
      </Body>
    </>
  );
}
