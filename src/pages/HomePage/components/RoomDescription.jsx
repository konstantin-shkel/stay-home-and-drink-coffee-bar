import React from 'react'
import styled from 'styled-components' 

import { BaseTextSection } from '../../../components/BaseTextSection';
import { BaseBorderColor, BaseTextFamily } from '../../../components/constants';

const Wrapper = styled(BaseTextSection)`
  display: flex;
  flex-wrap: wrap;
  border-bottom: 1px solid ${BaseBorderColor};
  padding-bottom: 10px;
  padding-top: 10px;
`;

const Name = styled.div`
  flex: 0 0 50%;
  font-size: 32px;
  font-weight: bold;
  line-height: 38px;
  align-self: flex-end;
`;

const Description = styled.div`
  flex: 0 0 22%;
  word-wrap: break-word;
  padding-right: 3%;
`;

const Link = styled.div`
  flex: 0 0 25%;
  align-self: flex-end;
`;

const JoinLink = styled.a`
  color: #008ACE;
  font-size: 16px;
  font-family: ${BaseTextFamily};
  text-decoration: none;
  display: block;
`;

export function RoomDescription({ name, description, joinLink}) {  
  return (
    <Wrapper>
      <Name>{name}</Name>
      <Description>{description}</Description>
      <Link>{ joinLink
        ? <JoinLink href={joinLink} target="_blank">JOIN →</JoinLink>
        : 'COMMING SOON'
      }
      </Link>
    </Wrapper>
  );
}
