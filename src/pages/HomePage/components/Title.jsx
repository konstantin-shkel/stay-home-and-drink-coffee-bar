import React from 'react'
import styled from 'styled-components' 

import { BaseTextSection } from '../../../components/BaseTextSection';
import { TitleTextSection } from '../../../components/TitleTextSection';

const TitleText = styled(TitleTextSection)`
  display: flex;
  flex-wrap: wrap;
`;

const SubTitleText = styled.div`
  font-size: 22px;
  line-height: 28px;
`;

const Ellipse = styled.div`
  background-color: #5A9832;
  width: 15px;
  height: 15px;
  -moz-border-radius: 50%;
  -webkit-border-radius: 50%;
  border-radius: 50%;
  align-self: flex-end;
  margin-bottom: 22px;
  margin-left: 10px;
`;

export function Title(props) {  
  return (
    <BaseTextSection>
      <TitleText>
        Coffee Online
        <Ellipse />
      </TitleText>
      <SubTitleText>🏡 Working from home</SubTitleText>
    </BaseTextSection>
  );
}
