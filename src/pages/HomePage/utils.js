import { advices } from '../../constants';

export const getRandomAdvice = () => {
  const countOfAdvices = advices.length;
  const adviceIndex = Math.floor(Math.random() * countOfAdvices);
  const randomAdvice = advices[adviceIndex];
  return {
    number: adviceIndex + 1,
    title: randomAdvice.title,
    text: randomAdvice.text
  };
}