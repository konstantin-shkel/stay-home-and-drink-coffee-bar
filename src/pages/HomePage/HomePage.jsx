import React from 'react';
import styled from 'styled-components';

import { Advice } from '../../components/Advice';
import { NavigationLink } from '../../components/NavigationLink';
import { BaseTextColor, BaseBorderColor, BaseTextFamily } from '../../components/constants';

import { rooms } from '../../constants';

import { getRandomAdvice } from './utils';
import { RoomDescription } from './components/RoomDescription';
import { Title } from './components/Title';

const Header = styled.div`
  display: flex;
`;

const HeaderFirstColumn = styled.div`
  flex: 0 0 67%;
  padding-right: 3%;
`;

const HeaderSecondColumn = styled.div`
  flex: 0 0 30%;
`;

const HeaderText = styled.p`
  padding-left: 39%;
  margin-top: 0;
  color: ${BaseTextColor};
  font-family: ${BaseTextFamily};
  font-size: 16px;
  line-height: 24px;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
  margin-bottom: 20px;
`;

const Body = styled.div`
  display: flex;
`;

const BodyFirstColumn = styled.div`
  flex: 0 0 26%;
`;

const BodySecondColumn = styled.div`
  flex: 0 0 74%;
  border-top: 1px solid ${BaseBorderColor};
`;

const RoomsTableWrapper = styled.div`
  width: 78%;
`;

export function HomePage() {
  const randomAdvice = getRandomAdvice();
  return (
    <>
      <Header>
        <HeaderFirstColumn>
          <HeaderText>Hi there! It’s gonna be a great day, isn’t it? Let’s make a great start with a cup of coffee while speaking to your colleagues! It’s almost like coffee-break in the office, but a bit better :)</HeaderText>
          <TitleWrapper>
            <Title />
          </TitleWrapper>
          <HeaderText>Video–chat rooms are for EPAMers only. All round the world. Any topic you want. This is not a project meeting - just enjoy conversation, stay yourself or even <a href="https://snapcamera.snapchat.com/" target="_blank">put on a mask</a> to make it even less formal ;)</HeaderText>
          <HeaderText>But please, keep yourself from making it too informal. Remember that there might be your boss 👀in the room and you are still under the limits of company rules.</HeaderText>
        </HeaderFirstColumn>
        <HeaderSecondColumn>
          <Advice number={randomAdvice.number} title={randomAdvice.title} text={randomAdvice.text} />
          <NavigationLink to="/advices">READ ALL →</NavigationLink>
        </HeaderSecondColumn>          
      </Header>
      <Body>
        <BodyFirstColumn></BodyFirstColumn>
        <BodySecondColumn>
          <RoomsTableWrapper>
          {
            rooms.map(({ id, name, description, joinLink}) => (
              <RoomDescription key={id} name={name} description={description} joinLink={joinLink} />
            ))
          }
          </RoomsTableWrapper>
        </BodySecondColumn>
      </Body>      
    </>
  );
}
