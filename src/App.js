import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";

import './App.css';

import { PageContainer } from './components/PageContainer';
import { HomePage } from './pages/HomePage';
import { AdvicesPage } from './pages/AdvicesPage';
import { NotFoundPage } from './pages/NotFoundPage';

function App() {
  return (
    <BrowserRouter>
      <PageContainer>
        <Switch>
          <Route exact path="/"><HomePage /></Route>
          <Route exact path="/advices"><AdvicesPage /></Route>
          <Route path="*"><NotFoundPage /></Route>
        </Switch>
      </PageContainer>
    </BrowserRouter>
  );
}

export default App;
